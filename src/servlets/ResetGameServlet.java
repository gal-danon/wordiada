package servlets;

import bl.ServerManager;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by Gald on 12-Aug-17.
 */
public class ResetGameServlet  extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String [] index = request.getQueryString().substring(1,request.getQueryString().length()).split("&");
        ServerManager serverManager = ServerManager.getInstance();
        serverManager.resetGame(Integer.valueOf(index[0]));
    }
}

