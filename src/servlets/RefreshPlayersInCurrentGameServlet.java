package servlets;

import bl.Engine;
import bl.ServerManager;
import com.google.gson.Gson;
import domain.HumanMyPlayer;
import domain.MyPlayer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

/**
 * Created by user on 04/08/2017.
 */
public class RefreshPlayersInCurrentGameServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //returning JSON objects, not HTML
        response.setContentType("application/json");
        try (PrintWriter out = response.getWriter()) {
            Gson gson = new Gson();

            Engine engine = ServerManager.getInstance().getGame((Integer) request.getSession().getAttribute("engineIndex"));

            List<MyPlayer> playingPlayers = engine.getPlayingPlayers();
            String [][] playerList = new String[engine.getPlayingPlayers().size()][3];
            int i = 0;
            for (MyPlayer playingPlayer : playingPlayers) {
                playerList[i][0] = playingPlayer.getPlayerName();

                if (playingPlayer instanceof HumanMyPlayer) {
                    playerList[i][1] = "human";
                } else {
                    playerList[i][1] = "computer";
                }
                playerList[i][2] = String.valueOf(playingPlayer.getScore());
                i++;
            }

            String json = gson.toJson(playerList);
            out.println(json);
            out.flush();
        }
    }
}
