package servlets;

import bl.Engine;
import bl.ServerManager;
import domain.Point;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.AnchorPane;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static java.lang.Thread.sleep;

/**
 * Created by user on 12/08/2017.
 */
public class ComputerServlet extends HttpServlet{
        protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
            Engine engine = ServerManager.getInstance().getGame((Integer) request.getSession().getAttribute("engineIndex"));
            if ((engine.checkHowManyUnHiddenSquares() + engine.checkHowManyEmptySquares()) < (engine.getBoardSize() * engine.getBoardSize())) {
                engine.roll();

                //Update roll num if num of hidden squares is smaller
                if (engine.checkHowManyHiddenSquares() < engine.getRoll())
                    engine.setRoll(engine.checkHowManyHiddenSquares());

                engine.setStatus("roll");

                engine.computerChoosingTails();

                engine.setStatus("discover");

                engine.chooseRandomCards();

                String wordResult = engine.verifyWord();

                while (wordResult != null && engine.getCurrentPlayerTurnsLeft() >= 0) {

                    engine.chooseRandomCards();

                    wordResult = engine.verifyWord();
                }

                // Success, finish turn
                if (wordResult == null) {
                    engine.finishTurn(engine.createWordAndAddToPlayerList());
                }
                // Fail, finish turn
                else if (engine.getCurrentPlayerTurnsLeft() < 0) {
                    engine.finishTurn(0);
                }
            }
        }

}
