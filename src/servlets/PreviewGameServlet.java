package servlets;

import bl.Engine;
import bl.ServerManager;
import com.google.gson.Gson;
import domain.ComputerMyPlayer;
import domain.HumanMyPlayer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by Gald on 12-Aug-17.
 */
public class PreviewGameServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("application/json");
        try (PrintWriter out = response.getWriter()) {
            String [] index = request.getQueryString().substring(1,request.getQueryString().length()).split("&");
            Gson gson = new Gson();
            String json = gson.toJson(ServerManager.getInstance().getLoadedGames().get(Integer.valueOf(index[0])).getBoardSize());
            out.println(json);
            out.flush();
        }

    }
}