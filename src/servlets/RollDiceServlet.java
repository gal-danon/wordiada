package servlets;

import bl.Engine;
import bl.ServerManager;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by user on 08/08/2017.
 */
public class RollDiceServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Engine engine = ServerManager.getInstance().getGame((Integer) request.getSession().getAttribute("engineIndex"));
            if ((engine.checkHowManyUnHiddenSquares() + engine.checkHowManyEmptySquares()) < (engine.getBoardSize() * engine.getBoardSize()))
            engine.roll();

            //Update roll num if num of hidden squares is smaller
            if (engine.checkHowManyHiddenSquares() < engine.getRoll())
                engine.setRoll(engine.checkHowManyHiddenSquares());
            engine.setStatus("roll");

    }
}
