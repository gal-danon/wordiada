package servlets;

import bl.Engine;
import bl.ServerManager;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by user on 12/08/2017.
 */
public class StartServlet  extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Engine engine = ServerManager.getInstance().getGame((Integer) request.getSession().getAttribute("engineIndex"));

        engine.setStatus("started");
    }

}
