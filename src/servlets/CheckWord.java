package servlets;

import bl.Engine;
import bl.ServerManager;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by user on 11/08/2017.
 */
public class CheckWord extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("application/json");
        try (PrintWriter out = response.getWriter()) {
            Gson gson = new Gson();
            Engine engine = ServerManager.getInstance().getGame((Integer) request.getSession().getAttribute("engineIndex"));

            String wordResult = engine.verifyWord();

            // Success, finish turn
            if (wordResult == null) {
                wordResult = "hurray!!! The word exists in the dictionary, you are the woman (or man)!";
                engine.finishTurn(engine.createWordAndAddToPlayerList());
            }
            // Fail, finish turn
            else if (engine.getCurrentPlayerTurnsLeft() < 0) {
                engine.finishTurn(0);
            }
            else {
                engine.setStatus("secondTry");
            }

            out.println(gson.toJson(wordResult));
            out.flush();
        }
    }
}
