package servlets;

import bl.Engine;
import bl.ServerManager;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by user on 09/08/2017.
 */
public class DiscoverTilesServlet extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("application/json");
        try (PrintWriter out = response.getWriter()) {
            Gson gson = new Gson();
            Engine engine = ServerManager.getInstance().getGame((Integer) request.getSession().getAttribute("engineIndex"));
            BoardJson boardJson = new BoardJson(engine.getBoardSize(), engine.flipAndGetLetters(), null);
            engine.setStatus("discover");
            String json = gson.toJson(boardJson);
            out.println(json);
            out.flush();
        }
    }

    private class BoardJson {
        public int size;
        public char[] array;
        public String message;

        public BoardJson(int size, char[] array, String message) {
            this.size = size;
            this.array = array;
            this.message=message;
        }
    }
}

