package servlets;

import bl.ServerManager;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Gald on 31-Jul-17.
 */
public class LogoutServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = (String)request.getSession().getAttribute("username");
        ServerManager serverManager = ServerManager.getInstance();
        serverManager.deleteUserFromUsersList(username);

        request.getSession().invalidate();
        request.logout();
        request.getRequestDispatcher("/index.jsp").forward(request, response);
    }
}
