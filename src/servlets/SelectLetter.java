package servlets;

import bl.Engine;
import bl.ServerManager;
import com.google.gson.Gson;
import domain.Point;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * Created by user on 08/08/2017.
 */
public class SelectLetter extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("application/json");
        try (PrintWriter out = response.getWriter()) {
            Gson gson = new Gson();
            Engine engine = ServerManager.getInstance().getGame((Integer) request.getSession().getAttribute("engineIndex"));

            String[] coordinates = request.getQueryString().substring(1, request.getQueryString().length()).split("a");

            int row = Integer.parseInt(coordinates[0]);
            int col = Integer.parseInt(coordinates[1]);

            SelectLetterJson selectLetterJson = null;

            if (engine.getStatus().equals("discover") || engine.getStatus().equals("secondTry")) {
                Point p = new Point(row, col);

                try {
                    engine.checkIfSquareIsHidden(row, col);
                } catch (RuntimeException e) {
                    selectLetterJson = new SelectLetterJson(engine.buildWord(), "You chose an invalid tile");
                }

                if (selectLetterJson == null) {
                    if (!engine.isNumberExistInListAndDeleteIt(p)) {
                        engine.addLetter(p);
                    }

                    selectLetterJson = new SelectLetterJson(engine.buildWord(), null);
                }

                    String json = gson.toJson(selectLetterJson);
                out.println(json);
                out.flush();
            } else {

                if (engine.getCoord().size()< engine.getCurrentDiceResult())
                    engine.addCoord(row, col);
                else if(engine.getCoord().contains(new Point(row,col))){
                    engine.getCoord().remove(new Point(row,col));
                }
                else {
                    selectLetterJson = new SelectLetterJson(engine.buildWord(), "You can't choose more tiles..");
                    String json = gson.toJson(selectLetterJson);
                    out.println(json);
                    out.flush();
                }
            }
        }
    }

    private class SelectLetterJson{

        public String word;
        public String message;

        public SelectLetterJson(String word, String message) {
            this.word = word;
            this.message = message;
        }
    }
}
