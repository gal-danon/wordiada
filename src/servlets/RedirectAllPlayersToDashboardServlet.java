package servlets;

import bl.ServerManager;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Gald on 12-Aug-17.
 */
public class RedirectAllPlayersToDashboardServlet  extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String username = (String)request.getSession().getAttribute("username");
        Integer engineIndex = (Integer) request.getSession().getAttribute("engineIndex");

        ServerManager serverManager = ServerManager.getInstance();
        serverManager.deleteUserFromGamePlayers(username,engineIndex);

        request.getSession().setAttribute("engineIndex", null);
        request.getSession().setAttribute("boardsize",null );

        request.getRequestDispatcher("./dashboard.jsp").forward(request, response);
    }
}

