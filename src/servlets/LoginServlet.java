package servlets;

import bl.ServerManager;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by user on 08/07/2017.
 */
public class LoginServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter("username");
        boolean isComputer = request.getParameter("isComputer") != null ? true : false;

        ServerManager serverManager = ServerManager.getInstance();

        boolean result = serverManager.addPlayer(username, isComputer);

        if (!result) {
            request.setAttribute("errorMessage", "Duplicate name");
            request.getRequestDispatcher("/index.jsp").forward(request, response);
        }
        else {
            // go to 2nd page
            request.getSession().setAttribute("username",username);
            request.getSession().setAttribute("isComputer",isComputer);
            request.getRequestDispatcher("./dashboard.jsp").forward(request, response);
        }
    }

}
