package servlets;

import bl.Engine;
import bl.ServerManager;
import com.google.gson.Gson;
import domain.ComputerMyPlayer;
import domain.HumanMyPlayer;
import domain.MyPlayer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by Gald on 17-Jul-17.
 */
public class JoinGameServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int index = 0;

        //returning JSON objects, not HTML
        for (int i = 0; i < ServerManager.getInstance().getLoadedGames().size(); i++) {
            if (request.getParameter(String.valueOf(i)) != null) {
                index = i;
            }
        }

        ServerManager serverManager = ServerManager.getInstance();
        boolean result = serverManager.checkIfUserCanEnterGame(index);

        if (!result) {
           request.setAttribute("errorMessage", "The game is already active");
           request.getRequestDispatcher("./dashboard.jsp").forward(request, response);
        }

        else {
            request.getSession().setAttribute("engineIndex", index);

            Engine engine = ServerManager.getInstance().getGame((Integer) request.getSession().getAttribute("engineIndex"));

            boolean isComputer = (boolean) request.getSession().getAttribute("isComputer");
            String name = (String) request.getSession().getAttribute("username");

            if (isComputer) {
                engine.addPlayer(new ComputerMyPlayer(engine.getGameDescriptor().getStructure().getRetriesNumber(), name));
            } else {
                engine.addPlayer(new HumanMyPlayer(engine.getGameDescriptor().getStructure().getRetriesNumber(), name));
            }

            request.getSession().setAttribute("boardsize", engine.getBoardSize());
            request.getRequestDispatcher("./board.jsp").forward(request, response);
        }
    }

}
