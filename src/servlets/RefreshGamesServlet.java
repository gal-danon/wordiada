package servlets;

import bl.Engine;
import bl.ServerManager;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Set;

/**
 * Created by user on 08/07/2017.
 */
public class RefreshGamesServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //returning JSON objects, not HTML
        response.setContentType("application/json");
        try (PrintWriter out = response.getWriter()) {
            Gson gson = new Gson();
            List<Engine> gamesList = ServerManager.getInstance().getLoadedGames();
            String [][] gameListToTable= new String [gamesList.size()][7];

            int i=0;
            for (Engine engine : gamesList) {
                //Game title
                gameListToTable[i][0]=engine.getGameDescriptor().getDynamicPlayers().getGameTitle().toString();
                gameListToTable[i][0]= gameListToTable[i][0].substring(1,gameListToTable[i][0].length()-1);
                gameListToTable[i][0]= gameListToTable[i][0].replace(',',' ');

                //Uploaded by user
                gameListToTable[i][1]=engine.getUserName();

                //Board size
                gameListToTable[i][2]=engine.getBoardSize()+"x"+engine.getBoardSize();

                //Register Players/Needed Players
                byte totalPlayers=  engine.getGameDescriptor().getDynamicPlayers().getTotalPlayers();
                gameListToTable[i][3]=engine.getPlayingPlayers().size() + "/"+ Byte.toString(totalPlayers);

                //Dictionary name
                gameListToTable[i][4]=engine.getGameDescriptor().getStructure().getDictionaryFileName();

                //Letters In Deck
                gameListToTable[i][5]=engine.getCashierSize()+"";

                //Status
                if(engine.getStatus().equals("not_started"))
                    gameListToTable[i][6]="Not Started";
                else
                    gameListToTable[i][6]="Started";
                i++;
            }

            String json = gson.toJson(gameListToTable);
            out.println(json);
            out.flush();
        }
    }

}
