package servlets;

import bl.Engine;
import bl.ServerManager;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by Gald on 11-Aug-17.
 */
public class UpdateGeneralInfoDivServlet extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("application/json");
        try (PrintWriter out = response.getWriter()) {
            Gson gson = new Gson();
            Engine engine = ServerManager.getInstance().getGame((Integer) request.getSession().getAttribute("engineIndex"));
            String[] info = {engine.getGameType(),engine.getGoldFishMode(),String.valueOf(0),String.valueOf(engine.getBoardSize()), (String)request.getSession().getAttribute("username")};
            String json = gson.toJson(info);
            out.println(json);
            out.flush();
        }
    }
}
