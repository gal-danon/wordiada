package servlets;

import bl.ServerManager;
import com.sun.corba.se.spi.activation.Server;

import java.io.*;
import java.lang.reflect.Array;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Collection;

import java.util.Scanner;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
/**
 * Created by Gald on 10-Jul-17.
 */
@WebServlet("/upload")
@MultipartConfig(fileSizeThreshold = 1024 * 1024, maxFileSize = 1024 * 1024 * 5, maxRequestSize = 1024 * 1024 * 5 * 5)
public class FileUploadServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.sendRedirect("fileupload/form.html");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Part filePart1 = request.getPart("file1"); // Retrieves <input type="file" name="file">
        Part filePart2 = request.getPart("file2"); // Retrieves <input type="file" name="file">

        InputStream fileContent = filePart1.getInputStream();
        InputStream fileContent2 = filePart2.getInputStream();

        try {
            String userName=request.getSession().getAttribute("username").toString();
            ServerManager.getInstance().addGame(fileContent, fileContent2,userName,filePart2.getSubmittedFileName());
        }
        catch(Exception e) {
            request.setAttribute("FileErrorMsg", e.getMessage());
            request.getRequestDispatcher("/wordiada/dashboard.jsp").forward(request, response);
        }
        ///to change in the future
        request.getRequestDispatcher("/wordiada/dashboard.jsp").forward(request, response);

    }
}
