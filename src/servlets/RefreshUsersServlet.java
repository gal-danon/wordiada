package servlets;

import bl.ServerManager;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Set;

/**
 * Created by Gald on 13-Jul-17.
 */
public class RefreshUsersServlet  extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //returning JSON objects, not HTML
        response.setContentType("application/json");
        try (PrintWriter out = response.getWriter()) {
            Gson gson = new Gson();
            Set<String> gamesList = ServerManager.getInstance().getConnectedPlayers().keySet();
            String json = gson.toJson(gamesList);
            out.println(json);
            out.flush();
        }
    }
}
