package servlets;

import bl.Engine;
import bl.ServerManager;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by Gald on 12-Aug-17.
 */
public class EndGameServlet extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("application/json");
        try (PrintWriter out = response.getWriter()) {
            Gson gson = new Gson();
            Engine engine = ServerManager.getInstance().getGame((Integer) request.getSession().getAttribute("engineIndex"));
            String [] list=null;

            //all retired, one left
            if (engine.getPlayingPlayers().size()==1)
                engine.setIsGameFinishedProperty(true);

            //game is over- all retired OR game is finish
            if(engine.getIsGameFinishedProperty()) {
                list = engine.createEndGameList();
                engine.setStatus("endGame");
            }

            String json = gson.toJson(list);
            out.println(json);
            out.flush();
        }
    }
}
