package servlets;

import bl.Engine;
import bl.ServerManager;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;

/**
 * Created by user on 05/08/2017.
 */
public class RefreshGameStatus extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("application/json");
        try (PrintWriter out = response.getWriter()) {
            Gson gson = new Gson();

            Engine engine = ServerManager.getInstance().getGame((Integer) request.getSession().getAttribute("engineIndex"));

            boolean isMyTurn = engine.isMyTurn((String) request.getSession().getAttribute("username"));
            String json;
            char[] board = null;

            if (Arrays.asList("discover", "finishTurn", "started", "secondTry").contains(engine.getStatus())) {
                board = engine.getBoard();
            }

            json = gson.toJson(new GameStatus(isMyTurn, engine.getStatus(), generateMsg(engine),new BoardJson(engine.getBoardSize(), board),(boolean)request.getSession().getAttribute("isComputer") ? true : false));
            out.println(json);
            out.flush();
        }
    }

    private String generateMsg(Engine engine) {
        String playerName = engine.getPlayingPlayers().get(engine.getCurrentPlayerIndex()).getPlayerName();
        switch (engine.getStatus()) {

            case "started":
                return "It's " + playerName + " turn now";
            case "roll":
                return "Player " +  playerName + " rolled " + engine.getCurrentDiceResult();
            case "finishTurn": {
                String msg = "Player " + playerName +
                        " built the word " + engine.getWordForMessage() +
                        " and was ";
                String right = engine.verifyWord() == null ? "right" : "wrong";
                return msg + right;
            }
            case "secondTry" :
                return "Player " + playerName + " built word " + engine.getWordForMessage() + " and was wrong";
            default:
                return "";
        }
    }

    private class GameStatus {

        public boolean myTurn;
        public String action;
        public String msg;
        public BoardJson board;
        public boolean isComp;

        public GameStatus(boolean myTurn, String action, String msg, BoardJson board, boolean isComp) {
            this.myTurn = myTurn;
            this.action = action;
            this.msg = msg;
            this.board = board;
            this.isComp = isComp;
        }
    }

    private class BoardJson {
        public int size;
        public char[] array;

        public BoardJson(int size, char[] array) {
            this.size = size;
            this.array = array;
        }
    }
}