package utils;

/**
 * Created by user on 08/04/2017.
 */
public class StringUtils {

    public static String removeSpaces(String string) {
        return string.trim().replaceAll("\\s","");
    }
}
