package utils; /**
 * Created by user on 04/04/2017.
 */


import domain.Word;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.util.*;

public class FileParser {


    public static final String FOLDER_NAME = "/resources/";
    public static final String FILE_NAME = "moby dick.txt";
    public static final int IGNORE_CHAR = 65279;

    public static Map<String, Integer> readDictionary(InputStream inputStream, String fileName) {

        Scanner scanner = new Scanner(inputStream);
        Map<String,Integer> dictionary = new HashMap<>();
        String s;
        String[] split;

        // Now the scanner will read from the stream until he doesn't have next line = reached end of file
        while (scanner.hasNextLine()) {
            // nextLine() read a whole line into a string
            s = scanner.nextLine();

            if (s != null && !s.isEmpty() && (int)s.charAt(0) == IGNORE_CHAR) { continue; }

            split = s.split(" ");

            // Next, the split string created an array of strings with the words from the file.
            StringBuilder legalWord;
            for (String word : split) {
                legalWord = new StringBuilder(word);
                for (int i = 0; i < word.length(); i++) {

                    switch (word.charAt(i)) {
                        case '$':
                            legalWord.deleteCharAt(legalWord.indexOf("$"));
                            break;
                        case '%':
                            legalWord.deleteCharAt(legalWord.indexOf("%"));
                            break;
                        case ']':
                            legalWord.deleteCharAt(legalWord.indexOf("]"));
                            break;
                        case '[':
                            legalWord.deleteCharAt(legalWord.indexOf("["));
                            break;
                        case '}':
                            legalWord.deleteCharAt(legalWord.indexOf("}"));
                            break;
                        case '{':
                            legalWord.deleteCharAt(legalWord.indexOf("{"));
                            break;
                        case ')':
                            legalWord.deleteCharAt(legalWord.indexOf(")"));
                            break;
                        case '(':
                            legalWord.deleteCharAt(legalWord.indexOf("("));
                            break;
                        case '\'':
                            legalWord.deleteCharAt(legalWord.indexOf("\'"));
                            break;
                        case '"':
                            legalWord.deleteCharAt(legalWord.indexOf("\""));
                            break;
                        case '*':
                            legalWord.deleteCharAt(legalWord.indexOf("*"));
                            break;
                        case '+':
                            legalWord.deleteCharAt(legalWord.indexOf("+"));
                            break;
                        case '=':
                            legalWord.deleteCharAt(legalWord.indexOf("="));
                            break;
                        case '_':
                            legalWord.deleteCharAt(legalWord.indexOf("_"));
                            break;
                        case '-':
                            legalWord.deleteCharAt(legalWord.indexOf("-"));
                            break;
                        case ';':
                            legalWord.deleteCharAt(legalWord.indexOf(";"));
                            break;
                        case ':':
                            legalWord.deleteCharAt(legalWord.indexOf(":"));
                            break;
                        case '.':
                            legalWord.deleteCharAt(legalWord.indexOf("."));
                            break;
                        case ',':
                            legalWord.deleteCharAt(legalWord.indexOf(","));
                            break;
                        case '?':
                            legalWord.deleteCharAt(legalWord.indexOf("?"));
                            break;
                        case '!':
                            legalWord.deleteCharAt(legalWord.indexOf("!"));
                            break;
                        default:
                            break;
                    }
                }

                String key = legalWord.toString().toUpperCase();

                if (legalWord.length()>1) {
                    if (dictionary.containsKey(key.toString()))
                        dictionary.put(key, dictionary.get(key) + 1);
                    else
                        dictionary.put(key, 1);
                }
            }
        }
        return dictionary;
    }
}
