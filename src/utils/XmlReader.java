package utils;

import generated.GameDescriptor;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Gald on 31-Mar-17.
 */
public class XmlReader {

    public static <T> T read(InputStream inputStream, Class<T> tClass){

        JAXBContext jaxbContext = null;
        try {
            jaxbContext = JAXBContext.newInstance(tClass);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

            Object result = jaxbUnmarshaller.unmarshal(inputStream);
            return (T) result;
        } catch (JAXBException e) {
            throw new RuntimeException("Xml file is not valid.");
        }
    }
}

