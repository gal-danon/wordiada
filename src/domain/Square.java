package domain;


/**
 * Created by Gald on 31-Mar-17.
 */
public class Square {

    private int row,col;
    private MyLetter myLetter;
    private boolean isHidden;
    private boolean isEmpty;

    public Square(MyLetter myLetter,int i,int j) {
        this.myLetter = myLetter;
        this.isHidden = true;
        this.isEmpty = false;
        this.row=i;
        this.col=j;
    }

    public boolean isHidden() {
        return isHidden;
    }

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public int getCol() {
        return col;
    }

    public void setCol(int col) {
        this.col = col;
    }

    public void setHidden(boolean hidden) {
        isHidden = hidden;
    }

    public boolean isEmpty() {
        return isEmpty;
    }

    public void setEmpty(boolean empty) {
        isEmpty = empty;
    }

    public MyLetter getMyLetter() {
        return myLetter;
    }

    public void setMyLetter(MyLetter myLetter) {
        this.myLetter = myLetter;
    }

    @Override
    public String toString() {

        if (isEmpty) {
            return "|   |";
        }
        else if (isHidden) {
            return "| ? |";
        }
        else {
            return "| " + myLetter.toString() + " |";
        }
    }
}
