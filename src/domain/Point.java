package domain;

/**
 * Created by Gald on 09-Jun-17.
 */
public class Point {

    private int row,col;

    public Point(int row, int col){
        this.col=col;
        this.row=row;
    }

    @Override
    public String toString() {
        return "Point{" +
                "row=" + row +
                ", col=" + col +
                '}';
    }

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public int getCol() {
        return col;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Point point = (Point) o;

        if (row != point.row) return false;
        return col == point.col;
    }
    public void setCol(int col) {
        this.col = col;
    }
}
