package domain;

import generated.Player;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Gald on 31-Mar-17.
 */
abstract public class MyPlayer implements Serializable {

    private String name;
    private int wordCount=0, sumScoreOfWords=0, triesLeft, score;
    private Map<Word,Integer> historyOfWords;
    private Boolean isRetire = false;


    public MyPlayer(int triesLeft, String playerName) {
        this.wordCount = 0;
        this.sumScoreOfWords = 0;
        this.triesLeft = triesLeft;
        this.historyOfWords = new HashMap<>();
        this.isRetire = false;
        this.score = 0;
        this.name = playerName;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getWordCount() {
        return wordCount;
    }

    public void setWordCount(int wordCount) {
        this.wordCount = wordCount;
    }

    public int getSumScoreOfWords() {
        return sumScoreOfWords;
    }

    public void setSumScoreOfWords(int sumScoreOfWords) {
        this.sumScoreOfWords = sumScoreOfWords;
    }

    public int getTriesLeft() {
        return triesLeft;
    }

    public void setTriesLeft(int triesLeft) {
        this.triesLeft = triesLeft;
    }

    public Map<Word, Integer> getHistoryOfWords() {
        return historyOfWords;
    }

    public void setHistoryOfWords(Map<Word, Integer> historyOfWords) {
        this.historyOfWords = historyOfWords;
    }

    public Boolean getRetire() {
        return isRetire;
    }

    public void setRetire(Boolean retire) {
        isRetire = retire;
    }

    public String getPlayerName() {
        return name;
    }

    public String createStatisticsString(){
         StringBuilder statistics = new StringBuilder("    Word Count: " + wordCount + "\n");
        return statistics.toString();
     }

    public void addWord(Word word){
        if (historyOfWords!=null && historyOfWords.containsKey(word))
            historyOfWords.put(word,historyOfWords.get(word).intValue()+1);
        else
            historyOfWords.put(word,1);
    }
}
