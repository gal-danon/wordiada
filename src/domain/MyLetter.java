package domain;

import generated.Letter;



/**
 * Created by Gald on 31-Mar-17.
 */
public class MyLetter  {

    private Letter letter;

    public MyLetter(Letter letter) {
        this.letter = letter;
    }

    public Letter getLetter() {
        return letter;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MyLetter myLetter = (MyLetter) o;

        return letter != null ? letter.getSign().equals(myLetter.letter.getSign()) : myLetter.letter == null;
    }

    @Override
    public int hashCode() {
        return letter != null ? letter.getSign().hashCode() : 0;
    }

    @Override
    public String toString() {

        StringBuilder sb = new StringBuilder();

        for (String s : letter.getSign()) {
            sb.append(s);
        }

        return sb.toString();
    }
}
