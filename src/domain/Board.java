package domain;

import java.util.*;

/**
 * Created by Gald on 31-Mar-17.
 */
public class Board {

    private byte size;
    private Square[][] matrix;
    int numOfHiddenSquares;
    private List<Square> unHiddenSquares;
    private Random rand = new Random();

    public Board(byte size, Stack<MyLetter> shuffeledLetters,Map<MyLetter,Integer> lettersInDeck) {
        this.size = size;
        this.matrix = new Square[size][size];

        if (size * size > shuffeledLetters.size()) {
            throw new RuntimeException("There are not enough letters to fill the board");
        }

        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                MyLetter currentLetter = shuffeledLetters.pop();
                lettersInDeck.put(currentLetter, lettersInDeck.get(currentLetter)-1);
                matrix[i][j] = new Square(currentLetter,i,j);
            }

        }
        unHiddenSquares = new ArrayList<>();
        numOfHiddenSquares = size * size;
    }


    public byte getSize() {
        return size;
    }

    public int getNumOfHiddenSquares() {
        return numOfHiddenSquares;
    }

    public void setNumOfHiddenSquares(int newVal) {
        numOfHiddenSquares = newVal;
    }

    public void showSquare(int row, int col) {
        this.matrix[row][col].setHidden(false);
    }

    public void unShowSquare(int row, int col) {
        this.matrix[row][col].setHidden(true);
    }

    public String createBoardString() {

        StringBuilder boardStr = new StringBuilder();
        boardStr.append("     ");
        for (int i = 0; i < size; i++) {
            if (i+1<10) {
                if (i+1==9)
                    boardStr.append((i + 1) + "   ");
                else
                    boardStr.append((i + 1) + "    ");
            }
            else
                boardStr.append((i+1) + "   ");
        }

        int row = 1;
        boardStr.append("\n");
        for (int i = 0; i < size; i++) {
            for (int k = 0; k < 2; k++) {
                if (k % 2 == 0) {
                    boardStr.append("   ");
                    for (int j = 0; j < size; j++) {
                        boardStr.append("---  ");
                    }
                } else {

                    if(row>=10)
                        boardStr.append(row );
                    else
                        boardStr.append(row + " ");

                    for (int j = 0; j < size; j++) {
                        boardStr.append(matrix[i][j]);
                    }
                    row++;
                }
                boardStr.append("\n");
            }
        }

        boardStr.append("   ");
        for (int j = 0; j < size; j++) {
            boardStr.append("---  ");
        }
        boardStr.append("\n");

        return boardStr.toString();
    }


    public void checkIfSquareIsHidden(int row, int col) {
        if (matrix[row][col].isHidden()) {
            throw new RuntimeException();
        }
    }

    public void addToUnhiddenList(int row, int col) {
        unHiddenSquares.add(matrix[row][col]);
    }

    public void removeFromUnhiddenList(int row, int col) {
        Square toSearch= matrix[row][col];

        for (int i = 0; i <unHiddenSquares.size() ; i++) {
            if (toSearch==unHiddenSquares.get(i)) {
                unHiddenSquares.remove(i);
                break;
            }
        }

    }

    public void checkIfSquareIsEmpty(int row, int col) {
        if (matrix[row][col].isEmpty()) {
            throw new RuntimeException("You chose an empty square");
        }
    }

    public MyLetter getUnHiddenLetter(int row, int col) {

        Iterator<Square> iterator = unHiddenSquares.iterator();

        while (iterator.hasNext()) {
            Square current = iterator.next();
            if (current.getCol()==col && current.getRow()==row) {
                current.setEmpty(true);
                iterator.remove();
                return current.getMyLetter();
            }
        }

        return null;
    }

    public void updateBoardWithNewLettersFromCashier(List<MyLetter> takenLettersFromCashier) {
        for (MyLetter myLetter : takenLettersFromCashier) {
            boolean foundSquare = false;
            for (Square[] squares : matrix) {
                if (foundSquare) break;

                for (Square square : squares) {
                    if (square.isEmpty()) {
                        foundSquare = true;
                        square.setMyLetter(myLetter);
                        square.setEmpty(false);
                        square.setHidden(true);
                        numOfHiddenSquares += 1;
                        break;
                    }
                }
            }
        }
    }

    public void hideAllLetters() {
        for (Square[] squares : matrix) {
            for (Square square : squares) {
                square.setHidden(true);
            }
        }
    }

    public void emptyUnHiddenList() {
        Iterator<Square> iterator = unHiddenSquares.iterator();
        while (iterator.hasNext()){
            iterator.next();
            iterator.remove();
            numOfHiddenSquares = numOfHiddenSquares+1;
        }
    }

    public String getFirstHiddenRowCol() {
        StringBuilder rowCol=new StringBuilder();

        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                if (matrix[i][j].isHidden()) {
                    rowCol.append((i) + " " + (j));
                    return rowCol.toString();
                }
            }
        }
        return null;
    }

    public String createWordComboForComputer() {
        StringBuilder wordCombo=new StringBuilder();
        int wordSize=rand.nextInt(3) + 2;

        int firstLetter = rand.nextInt(unHiddenSquares.size());
        int secondLetter = rand.nextInt(unHiddenSquares.size());
        while(secondLetter==firstLetter)
            secondLetter = rand.nextInt(unHiddenSquares.size());

        wordCombo.append(unHiddenSquares.get(firstLetter).getMyLetter().getLetter().getSign().get(0));
        wordCombo.append(unHiddenSquares.get(secondLetter).getMyLetter().getLetter().getSign().get(0));

        if (wordSize==3 && unHiddenSquares.size()>=3 ) {
            int thirdLetter=rand.nextInt(unHiddenSquares.size());

            while(secondLetter==thirdLetter || firstLetter==thirdLetter )
                thirdLetter=rand.nextInt(unHiddenSquares.size());

            wordCombo.append(unHiddenSquares.get(thirdLetter).getMyLetter().getLetter().getSign().get(0));
        }

        if( wordCombo.toString().length()>0)
            return wordCombo.toString();
        else
            return null;
    }

    public Map<String, Integer> getNumOfLetters(boolean unhidden) {

        Map<String, Integer> numOfLetters = new HashMap<>();

        for (Square[] squares : matrix) {
            for (Square square : squares) {
                if (square.isEmpty() || (unhidden && square.isHidden())) continue;
                numOfLetters.compute(square.getMyLetter().getLetter().getSign().get(0), (k,v) -> v == null ? 1 : v +1);
            }
        }

        return numOfLetters;
    }

    public int getUnHiddenListSize() {
        return unHiddenSquares.size();
    }

    public int calcHowManyEmpty() {
        int counter=0;
        for (Square[] squares : matrix) {
            for (Square square : squares) {
                if (square.isEmpty())
                   counter++;
            }
        }

        return counter;
    }

    public boolean areAllShown() {
        for (Square[] squares : matrix) {
            for (Square square : squares) {
                if (square.isHidden())
                   return false;
            }
        }
        return true;
    }
    public Square getSquareByIndex(int row,int col){
        return matrix[row][col];
    }

    public char[] getLetters(boolean isGoldFishMode, String status) {

        char[] array = new char[matrix.length*matrix.length];
        int counter=0;

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix.length; j++) {
                Square square = matrix[i][j];
                if (square.isHidden()) {
                    array[counter] = '?';
                }
                else {
                    if(status.equals("finishTurn")&&isGoldFishMode){
                        array[counter] = '?';
                    }
                    else {
                        array[counter] = square.getMyLetter().getLetter().getSign().get(0).charAt(0);
                    }
                }
                counter++;
            }
        }

        return array;
    }
}


