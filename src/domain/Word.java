package domain;

import java.util.List;

/**
 * Created by Gald on 31-Mar-17.
 */
public class Word {
    List<MyLetter> word;
    int score;
    int segment;

    public Word(List<MyLetter> word, int score,int segment) {
        this.word = word;
        this.score = score;
        this.segment=segment;
    }

    public int getSegment() {
        return segment;
    }

    public void setSegment(int segment) {
        this.segment = segment;
    }

    public List<MyLetter> getWord() {
        return word;
    }

    public void setWord(List<MyLetter> word) {
        this.word = word;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    @Override
    public String toString() {

        return word.toString() ;
    }

    public String parseWord() {
        StringBuilder parsWord = new StringBuilder();
        for (MyLetter myLetter : word) {
            parsWord.append(myLetter.toString());
        }
        return parsWord.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Word word1 = (Word) o;

        return word != null ? word.equals(word1.word) : word1.word == null;
    }

    @Override
    public int hashCode() {
        return word != null ? word.hashCode() : 0;
    }
}
