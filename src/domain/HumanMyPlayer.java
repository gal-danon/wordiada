package domain;

import generated.Player;

/**
 * Created by Gald on 31-Mar-17.
 */
public class HumanMyPlayer extends MyPlayer {

    private String type;

    public HumanMyPlayer(int triesLeft, String name) {
        super(triesLeft, name);
    }

    public String getType() {
        return type;
    }

    @Override
    public String toString() {
        return "domain.MyPlayer Type: Human" + super.toString();
    }
}
