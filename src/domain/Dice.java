package domain; /**
 * Created by Gald on 31-Mar-17.
 */

import java.util.Random;
public class Dice {

    private int sizeOfDice;

    public void setCurrentDiceResult(int currentDiceResult) {
        this.currentDiceResult = currentDiceResult;
    }

    private int currentDiceResult;
    private Random rand = new Random();

    public Dice(byte sizeOfDice) {
        this.sizeOfDice = sizeOfDice;
        currentDiceResult=0;
    }

    @Override
    public String toString() {
        return "domain.Dice Result: " + currentDiceResult + "/n";
    }

    public int getCurrentDiceResult() {
        return currentDiceResult;
    }

    public void rollDice(){
        this.currentDiceResult = rand.nextInt(sizeOfDice-1) + 2;
    }
}
