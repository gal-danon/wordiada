package domain;

import utils.FileParser;

import java.io.InputStream;
import java.util.*;

/**
 * Created by Gald on 31-Mar-17.
 */
public class Dictionary {

    private Map<String,Integer> dictionary;
    private List<Map<String, Integer>> validWords;
    private int totalWordsInDictionary = 0;
    private int segment = 0;
    private SortedMap<String,Integer> sortedDictionary;

    private Map<String,Integer> dictionaryOrderedBySegment;
    private List<InnerWord>listOfWordsWithFrequancy;


    public Dictionary(InputStream inputStream, String fileName) {
        this.dictionary = FileParser.readDictionary(inputStream, fileName);
        validWords = new ArrayList<>();
        for (String validWord : dictionary.keySet()) {

            Map<String, Integer> validWordLetters = new HashMap<>();

            for (char c : validWord.toCharArray()) {
                String s = String.valueOf(c);
                validWordLetters.compute(s, (k,v) -> v == null ? 1 : v + 1);
            }

            validWords.add(validWordLetters);
        }

        createDictionaryOrderedBySegment();
    }

    public boolean isWordExist(String word){
        return dictionary.containsKey(word);
    }

    public Map<String, Integer> getDictionary() {
        return dictionary;
    }

    public List<Map<String, Integer>> getValidWords() {
        return validWords;
    }

    public int calcHowManyWordsInDictionary() {
        int countWords = 0;
        // Going over the map values and sum
        for (Integer integer : dictionary.values()) {
            countWords += integer;
        }
        return countWords;
    }

    public String[] getTenWordLowFreq() {
        String[] stringArray = new String[10];
        int index = 0;
        int sizeOfList = dictionaryOrderedBySegment.size();

        int size = 10;
        if(dictionaryOrderedBySegment.size()<size)
            size = dictionaryOrderedBySegment.size();
        for (int i = sizeOfList-1; i >=sizeOfList-10 ; i--) {
            stringArray[index] = listOfWordsWithFrequancy.get(i).word;
            index++;
        }
        return stringArray;
    }

    private class InnerWord{
        private String word;
        private Integer frequency;

        private InnerWord(String word, Integer frequency) {
            this.word = word;
            this.frequency = frequency;
        }
    }

    //This list contains all the entry of the map dictionary
    private void createSortedListOfInnerWord(){
       listOfWordsWithFrequancy = new LinkedList<>();
        for (Map.Entry<String, Integer> entry : dictionary.entrySet()) {
            InnerWord innerWord = new InnerWord(entry.getKey(), entry.getValue());
            listOfWordsWithFrequancy.add(innerWord);
        }

        //order the list
        sortListInnerWord(listOfWordsWithFrequancy);
    }

    private void sortListInnerWord(List<InnerWord> list){
        Collections.sort(list,  new Comparator<InnerWord>() {
            @Override
            public int compare(InnerWord p1, InnerWord p2) {
                return p2.frequency.compareTo(p1.frequency);
            }
        });
    }

    private int calcHowManyWordInSegment(int countOfWordInDictionary){
        return countOfWordInDictionary/3;
    }

    private void createDictionaryOrderedBySegment(){

        int countWordsInDictionary = calcHowManyWordsInDictionary();
        int segmentInMap = calcHowManyWordInSegment(countWordsInDictionary);
        int tempSum =0;
        int segment = 1;

        dictionaryOrderedBySegment = new HashMap<>();
       createSortedListOfInnerWord();
        for (InnerWord innerWord : listOfWordsWithFrequancy) {
            tempSum += innerWord.frequency;
            dictionaryOrderedBySegment.put(innerWord.word, segment);
            if (segment <3  && tempSum > segmentInMap) {
                segment++;
                tempSum = 0;
            }
        }
    }

    public Map<String, Integer> getDictionaryOrderedBySegment() {
        return dictionaryOrderedBySegment;
    }
}
