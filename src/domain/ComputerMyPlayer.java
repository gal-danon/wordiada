package domain;

import generated.Player;

/**
 * Created by Gald on 31-Mar-17.
 */

public class ComputerMyPlayer extends MyPlayer {

    private String type;

    public ComputerMyPlayer(int triesLeft, String name){
        super(triesLeft, name);
    }

    public String getType() {
        return type;
    }

    @Override
    public String toString() {
        return "domain.MyPlayer Type: computer " + super.toString();
    }

}
