package domain;


import java.util.*;

/**
 * Created by Gald on 31-Mar-17.
 */
public class CashierDesk {

    private Map<MyLetter,Integer> lettersInDeck;
    private Stack<MyLetter> shuffledLetters;
    private int squaresToFetch=0;

    public void setSquaresToFetch(int squaresToFetch) {
        this.squaresToFetch = squaresToFetch;
    }

    public Stack<MyLetter> getShuffledLetters() {
        return shuffledLetters;
    }

    public List<MyLetter> takeLettersFromCashier() {
        List<MyLetter> combination = new ArrayList<>();

        for (int i = 0; i <squaresToFetch && 0<shuffledLetters.size(); i++) {
            MyLetter currentLetter = shuffledLetters.pop();
            combination.add(currentLetter);
            lettersInDeck.put(currentLetter, lettersInDeck.get(currentLetter)-1);
        }
        return combination;
    }

    public void shuffleLetters(){

        shuffledLetters = new Stack<>();

        for (Map.Entry<MyLetter, Integer> pair : lettersInDeck.entrySet()) {
            for (int i = 0; i <pair.getValue() ; i++) {
                shuffledLetters.push(new MyLetter(pair.getKey().getLetter()));
            }
        }

        Collections.shuffle(shuffledLetters);
    }

    public String[] statusOfLetterInCashier(){
        String[] statusOfLetterInCashier = new String[lettersInDeck.size()];
        int index = 0;
        for (Map.Entry<MyLetter, Integer> myLetterIntegerEntry : lettersInDeck.entrySet()) {
            statusOfLetterInCashier[index]= (myLetterIntegerEntry.getKey().toString() + " - " + myLetterIntegerEntry.getValue() + "/" + shuffledLetters.size());
            index ++;
        }

        return statusOfLetterInCashier;
    }
    public int getTotalCountOfLetters() {
        return shuffledLetters.size();
    }

    public Map<MyLetter, Integer> getLettersInDeck() {
        return lettersInDeck;
    }

    public void setLettersInDeck(Map<MyLetter, Integer> lettersInDeck) {
        this.lettersInDeck = lettersInDeck;
    }
}
