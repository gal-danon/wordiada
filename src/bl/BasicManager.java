package bl;

import domain.MyPlayer;
import domain.Word;

import java.util.List;

/**
 * Created by user on 14/04/2017.
 */
public class BasicManager extends Manager{

    public BasicManager(String winnerAccordingTo, Boolean goldFishMode) {
        super(winnerAccordingTo, goldFishMode);
    }

    @Override
    public int calculateScore(Word word) {
        return 1;
    }

    public int calculateWinner(List<MyPlayer> players) {

        int max = -1;
        int maxIndex = -1;

        for (int i = 0; i < players.size(); i++) {
            if (players.get(i).getWordCount() > max) {
                max = players.get(i).getWordCount();
                maxIndex = i;
            }
            else if(players.get(i).getWordCount() == max)
            {
                maxIndex = -1;
            }
        }

        return maxIndex;
    }
}
