package bl;

import domain.*;
import domain.Dictionary;
import generated.*;
import generated.Letter;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import utils.XmlReader;

import java.io.InputStream;
import java.time.Duration;
import java.time.Instant;
import java.time.temporal.Temporal;
import java.util.*;

import static java.lang.Thread.sleep;

/**
 * Created by Gald on 31-Mar-17.
 */
public class Engine{

    final private BooleanProperty configurationValid = new SimpleBooleanProperty();
    final private BooleanProperty isGameFinished = new SimpleBooleanProperty();

    private InputStream inputStreamDictionary;
    private String dictionaryName;
    private List<Point> coord = new ArrayList<>();
    private List<Point> chosenWord = new ArrayList<>();
    private String status = "not_started";
    private String userName;
    private int currentPlayerIndex = 0;
    private boolean isGoldFishMode =false;
    private Random rand = new Random();
    private String wordForMessage;
    private int errorMessageNum;
    private boolean gameOver;
    private Dice dice;
    private Board board;
    private List<MyPlayer> playingPlayers= new ArrayList<MyPlayer>();
    private List<MyPlayer> retireList;
    private CashierDesk cashier;
    private Dictionary dictionary;
    private Manager manager;
    private int currTurn;
    private String stringXmlUser;
    private int countOfComputerPlayers;
    private GameDescriptor gameDescriptor;
    private String xmlPath;
    private int currentPlayerTurnsLeft;
    private InputStream inputStream;

    public BooleanProperty configurationProperty() {
        return configurationValid;
    }

    public String getWordForMessage() {
        return wordForMessage;
    }

    public int getErrorMessageNum() {
        return errorMessageNum;
    }

    public void addToRetireList(String userName){

        for (MyPlayer playingPlayer : playingPlayers) {
            if (playingPlayer.getPlayerName().equals(userName))
                retireList.add(playingPlayer);
        }
    }

    public InputStream getInputStream() {
        return inputStream;
    }

    public InputStream getInputStreamDictionary() {
        return inputStreamDictionary;
    }

    public String getDictionaryName() {
        return dictionaryName;
    }

    public String getUserName() {
        return userName;
    }

    public List<Point> getCoord() {
        return coord;
    }

    public int getCurrentPlayerIndex() {
        return currentPlayerIndex;
    }

    public List<Point> getChosenWord() {
        return chosenWord;
    }

    public void setChosenWord(List<Point> chosenWord) {
        this.chosenWord = chosenWord;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public BooleanProperty configurationValidProperty() {
        return configurationValid;
    }

    public GameDescriptor getGameDescriptor() {
        return gameDescriptor;
    }

    public void setConfigurationValid(boolean configurationValid) {
        this.configurationValid.set(configurationValid);
    }

    public Manager getManager() {
        return manager;
    }

    public boolean getIsGameFinishedProperty() {
        return isGameFinished.get();
    }

    public void setIsGameFinishedProperty(boolean value) {
         isGameFinished.setValue(value);
    }

    public void setCountOfComputerPlayers(int countOfComputerPlayers) {
        this.countOfComputerPlayers = countOfComputerPlayers;
    }

    public String getStatus() {
        return status;
    }

    public boolean addPlayer(MyPlayer player){

        if (playingPlayers.size() < gameDescriptor.getDynamicPlayers().getTotalPlayers()) {
            playingPlayers.add(player);

            // all players have joined
            if (playingPlayers.size() == gameDescriptor.getDynamicPlayers().getTotalPlayers()) {
                status = "started";
            }
            return true;
        }

        return false;
    }

    public boolean isGoldFishMode(){
        return manager.isGoldFishMode();
    }
    private Temporal startGameTime;

    public List<MyPlayer> getRetireList() {
        return retireList;
    }

    public void loadGame(InputStream inputStream, int numOfComputers,String userName) {
        configurationValid.setValue(false);
        isGameFinished.setValue(false);
        this.userName=userName;
        gameDescriptor = XmlReader.read(inputStream, GameDescriptor.class);
        currentPlayerTurnsLeft = gameDescriptor.getStructure().getRetriesNumber();
        this.inputStream=inputStream;
        status="not_started";
    }

    public void initVariables(){
        setCountOfComputerPlayers(0);
        initCashierDesk(gameDescriptor.getStructure().getLetters().getLetter(), gameDescriptor.getStructure().getLetters().getTargetDeckSize());
        initBoard(gameDescriptor.getStructure().getBoardSize(),cashier.getShuffledLetters(),cashier.getLettersInDeck());
        initCube(gameDescriptor.getStructure().getCubeFacets());
    }

   public void initDictionaryAndManager(InputStream inputStreamDictionary,String dictionaryName){
       gameDescriptor.getStructure().setDictionaryFileName(dictionaryName);
       dictionary = new Dictionary(inputStreamDictionary ,gameDescriptor.getStructure().getDictionaryFileName());
       initManager(gameDescriptor.getGameType());
       retireList = new ArrayList<>();
       currTurn = 0;
       this.inputStreamDictionary=inputStreamDictionary;
       this.dictionaryName=dictionaryName;
   }
    private void initManager(GameType gameType) {
            manager = new Manager(gameType.getWinnerAccordingTo(), gameType.isGoldFishMode());
    }

    private void initCashierDesk(List<Letter> letters,short targetCahierSize) {
        double sumOfFrequency = 0;
        cashier = new CashierDesk();
        for (Letter letter : letters) {
            sumOfFrequency += letter.getFrequency();
        }

        MyLetter myLetter;
        Map<MyLetter,Integer> myDeck = new HashMap<>();

        for (Letter letter : letters) {

            myLetter = new MyLetter(letter);

            if(myDeck.containsKey(myLetter)) {
                throw new RuntimeException("Duplicate letters found in XML: " + letter.getSign());
            }

            myDeck.put(myLetter, (int) Math.ceil((myLetter.getLetter().getFrequency() / sumOfFrequency) * targetCahierSize));
        }

        cashier.setLettersInDeck(myDeck);
        cashier.shuffleLetters();
    }

    private void initCube(byte cubeFacets) {
        dice = new Dice(cubeFacets);
    }

    private void initBoard(byte boardSize, Stack<MyLetter> shuffledStack,Map<MyLetter,Integer> lettersInDeck) {
        if(boardSize<5 || boardSize>50){
            throw new RuntimeException("Board size isn't valid, currently it's  " + boardSize +" and it should be between [5- 50]");
        }
        board = new Board(boardSize, shuffledStack,lettersInDeck);
    }

    public boolean isConfigurationValid() {
        return configurationValid.getValue();
    }

    public String gameStatus() {
        String status= board.createBoardString()+"\n"+
                "Number of Tiles in the Cashier: "+cashier.getTotalCountOfLetters()+"\n"+
                "Player "+ (currentPlayerIndex+1) +" turn:"+"\n";
        return status;
    }

    public boolean isGameOver() {
        return gameOver;
    }

    public int roll() {
        dice.rollDice();
        return getCurrentDiceResult();
    }

    public int getCurrentDiceResult() {
        return dice.getCurrentDiceResult();
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void showSquare(int row, int col) {
        board.showSquare(row,col);
        board.addToUnhiddenList(row,col);
        updateNumOfHiddenSquares(-1);
    }

    public void unShowSquare(int row, int col) {

        board.unShowSquare(row,col);
        board.removeFromUnhiddenList(row,col);
        updateNumOfHiddenSquares(+1);
    }

    public String showBoard() {
        return board.createBoardString();
    }

    public int getCurrentPlayerTurnsLeft() {
        return currentPlayerTurnsLeft;
    }

    public void finishTurn(int score) {

        coord = new ArrayList<>();
        if (score > 0) {
            board.updateBoardWithNewLettersFromCashier(cashier.takeLettersFromCashier());
            playingPlayers.get(currentPlayerIndex).setScore(playingPlayers.get(currentPlayerIndex).getScore()+score);
        }
        //check if the GAME IS OVER
        if (!canPlayOn())
            setIsGameFinishedProperty(true);

        if(cashier.getShuffledLetters().size()==0)
            setIsGameFinishedProperty(true);

        if (board.getUnHiddenListSize() == getBoardSize()*getBoardSize()) {
            setIsGameFinishedProperty(true);
        }

        currTurn++;
        if (manager.isGoldFishMode()) {
            board.hideAllLetters();
            board.emptyUnHiddenList();
        }
        currentPlayerIndex = (currentPlayerIndex + 1) % playingPlayers.size();
        currentPlayerTurnsLeft = gameDescriptor.getStructure().getRetriesNumber();
        System.out.println("1 clear chosen word");
        setStatus("finishTurn");
        chosenWord = new ArrayList<>();
    }

    public String verifyWord() {

       currentPlayerTurnsLeft--;
        String wordOfUser = buildWord();
        wordForMessage = wordOfUser;
        Map<String, Integer> numOfLetters = board.getNumOfLetters(true);
        Map<String, Integer> wordChars = new HashMap<>();

        for (char c : wordOfUser.toCharArray()) {
            wordChars.compute(String.valueOf(c), (k, v) -> v == null ? 1 : v + 1);
        }

        boolean isValid = true;
        for (String letter : wordChars.keySet()) {

            if (numOfLetters.get(letter) == null || wordChars.get(letter) > numOfLetters.get(letter)) {
                isValid = false;
                break;
            }
        }

        if (!isValid) {
            errorMessageNum = 2;
            chosenWord = new ArrayList<>();
            System.out.println("2 clear chosen word");
            return "You chose invalid letter/s in your combination of word!";
        }
        if(!dictionary.isWordExist(wordOfUser)) {
            errorMessageNum = 1;
            chosenWord = new ArrayList<>();
            System.out.println("3 clear chosen word");


            if(currentPlayerTurnsLeft>=0) {
                int turnLeft = currentPlayerTurnsLeft + 1;
                status = "secondTry";
                return "Your combination of word doesn't exist in the dictionary. You have " + turnLeft + " more tries.";
            }
            else{
                return "Your combination of word doesn't exist in the dictionary. Your turn is over.";
            }
        }
        return null;
    }

    public int checkHowManyHiddenSquares() {
        return board.getNumOfHiddenSquares();
    }

    public void updateNumOfHiddenSquares(int i) {
        board.setNumOfHiddenSquares(board.getNumOfHiddenSquares()+i);
    }

    public void checkIfValidRowCol(int row, int col){
        if (row <= board.getSize() && row>0)
            if (col <= board.getSize() && col>0)
                return;
             else
                 throw new RuntimeException("Square with column " + col + " is not valid");
         else
            throw new RuntimeException("Square with row " + row + " is not valid");
    }

    public void checkIfSquareIsHidden(int row,int col) {
       board.checkIfSquareIsHidden(row,col);
    }

    public Square getSquare(int row,int col){
        return board.getSquareByIndex(row,col);
    }

    public int getHowManyTurnsPerPlayer() {
        return playingPlayers.get(0).getTriesLeft();
    }

    public int createWordAndAddToPlayerList() {

        String wordOfUser = buildWord();
        List<Point> chosenWordPoints = chosenWord;
        int score=0;

       List <MyLetter> letterList=new ArrayList<>();
        for (int i = 0; i <chosenWordPoints.size() ; i++) {
                MyLetter l= board.getUnHiddenLetter(chosenWordPoints.get(i).getRow(),chosenWordPoints.get(i).getCol());
                letterList.add(l);
                score+=l.getLetter().getScore();
        }
        Word word= new Word(letterList,score,dictionary.getDictionaryOrderedBySegment().get(wordOfUser));

        cashier.setSquaresToFetch( word.getWord().size());

        //Add the word to the list of words of the player
        playingPlayers.get(currentPlayerIndex).addWord(word);
        //update SumScoreOfWords of the player
        int seg= word.getSegment();
        playingPlayers.get(currentPlayerIndex).setSumScoreOfWords(playingPlayers.get(currentPlayerIndex).getSumScoreOfWords()+(score*seg));
        playingPlayers.get(currentPlayerIndex).setWordCount(playingPlayers.get(currentPlayerIndex).getWordCount()+1);
        return manager.calculateScore(word);

    }

    public void updateScoreOfPlayer(int score) {
        MyPlayer currentPlayer = playingPlayers.get(currentPlayerIndex);
        currentPlayer.setWordCount(currentPlayer.getWordCount()+1);
        currentPlayer.setScore(currentPlayer.getScore() + score);
    }

    public String createStatistics(){

        long secondsFromStart = Duration.between( startGameTime,Instant.now()).getSeconds();

        long seconds = secondsFromStart % 60;
        long minutes = (secondsFromStart - seconds) / 60;

        StringBuilder statistics = new StringBuilder();
        statistics.append("Num of turns until now: " + currTurn + "\n" +
                "Time passed from start " + minutes + ":" + seconds + "\n" +
                "Number of Tiles in the Cashier: "+cashier.getTotalCountOfLetters()+"\n"+
                "Status of each letter in cashier:" + "\n");
        statistics.append(cashier.statusOfLetterInCashier());

        for (int i = 0; i <playingPlayers.size() ; i++) {
            MyPlayer player= playingPlayers.get(i);
            statistics.append("Player " + (i+1) + ":");
            statistics.append(player.createStatisticsString());
            statistics.append(createWordsStringOfPlayer(i));
        }

        return statistics.toString();
    }

    public void checkIfSquareIsEmpty(int row, int col) {
        board.checkIfSquareIsEmpty(row,col);
    }

    public boolean verifyHumanPlayers() {
        for (MyPlayer playingPlayer : playingPlayers) {
            if(playingPlayer instanceof HumanMyPlayer){
                return true;
            }
        }
        return false;
    }

    public boolean isCurrPlayerHuman() {
       return (playingPlayers.get(currentPlayerIndex) instanceof HumanMyPlayer);
    }

    public String calculateComputerRowCol() {
        String inputComputer= board.getFirstHiddenRowCol();
        return inputComputer;
    }

    public String getComputerCombo() {
        return board.createWordComboForComputer();
    }

    public boolean canPlayOn() {

        //when cash is empty the Game is over!!!!
        if (cashier.getTotalCountOfLetters()==0)
            return false;

        if (validWordsLeft()) return true;

        return false;
    }

    private boolean validWordsLeft() {
        Map<String, Integer> numOfLetters = board.getNumOfLetters(false);
        List<Map<String, Integer>> validWords = dictionary.getValidWords();

        for (Map<String, Integer> validWord : validWords) {
            boolean isValid = true;
            for (String letter : validWord.keySet()) {

                if (numOfLetters.get(letter) == null || validWord.get(letter) > numOfLetters.get(letter)) {
                    isValid = false;
                    break;
                }
            }
            if (isValid==true) {
                return true;
            }
        }
        return false;
    }

    public int calculateWinner() {

        return manager.calculateWinner(playingPlayers);
    }

    public int checkHowManyUnHiddenSquares() {
        return board.getUnHiddenListSize();
    }

    public int checkHowManyEmptySquares() {
        return board.calcHowManyEmpty();
    }

    public int getBoardSize() {
        return board.getSize();
    }

    public String getGameType() {
        return manager.getWinnerAccordingTo();
    }

    public String getGoldFishMode() {
        if (manager.isGoldFishMode())
            return "True";
        else
            return "False";
    }

    public String[] calcAllPlayersScore() {
        String[] sb=new String[playingPlayers.size()];
        int index = 0;

        for (MyPlayer playingPlayer : playingPlayers) {
          sb[index]=playingPlayer.getPlayerName()+": " + playingPlayer.getScore();
          index++;
        }
        return sb;
    }

    public String[] getTenWordsLowFreq() {
        return dictionary.getTenWordLowFreq();
    }

    public String[] getCashierLetters() {
        return cashier.statusOfLetterInCashier();
    }

    public Square getSquareByIndex(int row, int col){
        return board.getSquareByIndex(row,col);
    }

    public List<MyPlayer> getPlayingPlayers() {
        return playingPlayers;
    }

    public void removePlayer(MyPlayer player){
        playingPlayers.remove(player);
    }

    public void removePlayerByName(String playerName){
        addToRetireList(playerName);
        for (MyPlayer playingPlayer : playingPlayers) {
            if(playingPlayer.getPlayerName().equals(playerName)) {
                removePlayer(playingPlayer);
                break;
            }
        }
    }

    public void sortPlayingPlayerByScore(){
        Collections.sort(playingPlayers,  new Comparator<MyPlayer>() {
            @Override
            public int compare(MyPlayer p1, MyPlayer p2) {
                if (p2.getScore() > p1.getScore()) {
                    return 1;
                }
                return -1;
            }
        });
    }

    public int getCashierSize(){
            return cashier.getTotalCountOfLetters();
    }

    public String[] createPlayerWordCollection() {
        Map<Word, Integer> mapOfWords = playingPlayers.get(currentPlayerIndex).getHistoryOfWords();
        String[] res = new String[mapOfWords.size()];
        if (mapOfWords.size() == 0){
            return res;
        }

        StringBuilder sb=new StringBuilder();
        int index = 0,seg,currVal;

        for (Map.Entry<Word,Integer> entry : mapOfWords.entrySet()) {
            sb.delete(0,sb.length());
            sb.append("Word: "+entry.getKey().parseWord() +" Count: "+ entry.getValue());
            if(manager.getWinnerAccordingTo().equals("WordScore")) {
                currVal=entry.getKey().getScore();
                seg=dictionary.getDictionaryOrderedBySegment().get(entry.getKey().parseWord());
                sb.append(" Word Value: " + currVal + " Segment: " + seg + " Word Score: "+currVal*seg);
            }
            res[index]=sb.toString();
            index++;
        }

        return res;
    }

    public String getComputerComboTask() {
        String computerComboTask=null;
        return computerComboTask;
    }

    public boolean isMyTurn(String playerName) {

        for (int i = 0; i < playingPlayers.size(); i++) {
            if (playingPlayers.get(i).getPlayerName().equals(playerName)) {
                return currentPlayerIndex == i;
            }
        }

        return false;
    }

    public void addCoord(int row, int col) {
        coord.add(new Point(row,col));
        for (Point point : coord) {
            System.out.println(point);
        }
    }

    public char[] flipAndGetLetters() {

        for (Point point : coord) {
            showSquare(point.getRow(), point.getCol());
        }

        return board.getLetters(isGoldFishMode, status);
    }

    public boolean isNumberExistInListAndDeleteIt(Point p) {
        System.out.println("remove " + p + " from chosen word");
                return chosenWord.remove(p);
    }

    public void addLetter(Point p) {
        System.out.println("add " + p + " to chosen word");
        chosenWord.add(p);
    }

    public String buildWord() {
        StringBuilder sb = new StringBuilder();
        for (Point point : chosenWord) {
            sb.append(this.getSquareByIndex(point.getRow(),point.getCol()).getMyLetter().toString());
        }
        return sb.toString();
    }

    public String[] createCurrentPlayerInfoString() {
        String [] result=new String[7];

        result[0]=playingPlayers.get(currentPlayerIndex).getPlayerName();

        if(playingPlayers.get(currentPlayerIndex) instanceof HumanMyPlayer)
            result[1]="Human";
        else
            result[1]="Computer";

        result[2]=String.valueOf(playingPlayers.get(currentPlayerIndex).getScore());
        result[3]=String.valueOf(playingPlayers.get(currentPlayerIndex).getWordCount());
        result[4]=String.valueOf(currentPlayerTurnsLeft);
        result[5]=String.valueOf(getRetriesNumber());
        result[6]=String.valueOf(currTurn);

        return result;
    }

    public void chooseRandomCards() {
        List<Point> res= new ArrayList<>();
        int letterPoint , wordSize=rand.nextInt(2) + 2;;

        if(coord.size()<wordSize)
            wordSize=coord.size();

        for (int i = 0; i <wordSize ; i++) {

            letterPoint = rand.nextInt(coord.size());
            while(res.contains(coord.get(letterPoint))){
                letterPoint = rand.nextInt(coord.size());
            }
            res.add(coord.get(letterPoint));
        }
        chosenWord = res;
        wordForMessage = buildWord();
    }

    public void computerChoosingTails(){
        String userInput;
        String[] split;
        int row, col;
        //choosing the tiles
        for (int i = 0; i < getCurrentDiceResult(); i++) {

            userInput = calculateComputerRowCol();
            split = userInput.split(" ");
            row = Integer.valueOf(split[0]);
            col = Integer.valueOf(split[1]);
            Point p = new Point(row, col);
            showSquare(p.getRow(), p.getCol());
            coord.add(p);
        }
    }

    public int getRetriesNumber(){
       return gameDescriptor.getStructure().getRetriesNumber();
    }

    public String[] createEndGameList() {
        List<String> result = new ArrayList<>();
        int i=0;
        sortPlayingPlayerByScore();

        for (MyPlayer playingPlayer : playingPlayers) {
            result.add(playingPlayer.getPlayerName()+", with the score of: "+ playingPlayer.getScore()+"\n");
            i++;
        }

        if (retireList.size()!=0){
            result.add("Retired:\n");
            for (MyPlayer myPlayer : retireList) {
                result.add(myPlayer.getPlayerName()+", with the score of: "+ myPlayer.getScore()+"\n");
            }
        }

        return result.toArray(new String[result.size()]);
    }

    public char[] getBoard() {
        return board.getLetters(isGoldFishMode,status) ;
    }

    public int getRoll() {
        return getCurrentDiceResult();
    }

    public void setRoll(int roll) {
        dice.setCurrentDiceResult(roll);
    }
}