package bl;

import domain.MyPlayer;
import domain.Word;

import javax.xml.bind.annotation.XmlAttribute;
import java.io.Serializable;
import java.util.List;

/**
 * Created by user on 01/04/2017.
 */
public class Manager {

    protected String winnerAccordingTo;
    protected Boolean goldFishMode=false;

    public Manager(String winnerAccordingTo, Boolean goldFishMode) {
        this.winnerAccordingTo = winnerAccordingTo;

        if (goldFishMode==null || goldFishMode==false)
            this.goldFishMode =false;
        else
            this.goldFishMode =true;
    }

    public String getWinnerAccordingTo() {
        return winnerAccordingTo;
    }

    public boolean isGoldFishMode() {
        return goldFishMode;
    }

    public int calculateScore(Word word){

        if (winnerAccordingTo.equals("WordCount"))
            return 1;
        else{
            return word.getScore()*word.getSegment();
        }
    }

    public int calculateWinner(List<MyPlayer> players){
        int max = -1;
        int maxIndex = -1;

            for (int i = 0; i < players.size(); i++) {
                if (players.get(i).getScore() > max) {
                    max = players.get(i).getScore();
                    maxIndex = i;
                }
                else if(players.get(i).getScore() == max)
                {
                    maxIndex = -1;
                }
            }
            return maxIndex;
    }
}
