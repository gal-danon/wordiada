package bl;

import jdk.internal.util.xml.XMLStreamException;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by user on 08/07/2017.
 */
public class ServerManager {

    private Map<String, String> connectedPlayers = new HashMap<>();

    private List<Engine> loadedGames= new ArrayList<>();

    public Map<String, String> getConnectedPlayers() {
        return connectedPlayers;
    }

    private static final ServerManager serverManager = new ServerManager();

    private ServerManager() {}

    public static ServerManager getInstance() {
        return serverManager;
    }

    public List<Engine> getLoadedGames() {
        return loadedGames;
    }

    public boolean addPlayer(String name, boolean isComputer) {
        if (connectedPlayers.containsKey(name)) {
            return false;
        }

        connectedPlayers.put(name, isComputer ? "COMPUTER" : "HUMAN");
        return true;
    }

    public boolean addGame(InputStream inputStreamXml,InputStream inputStreamDictionary,String userName, String dictionaryName) {

          Engine engine = new Engine();

          engine.loadGame(inputStreamXml, 0,userName);
          engine.initVariables();
          engine.initDictionaryAndManager(inputStreamDictionary,dictionaryName);

          loadedGames.add(engine);
          return true;

    }

    public Engine getGame(int i) {
        return  loadedGames.get(i);
    }

    public boolean checkIfUserCanEnterGame(int i) {

        //Register Players/Needed Players
        byte totalPlayers=  loadedGames.get(i).getGameDescriptor().getDynamicPlayers().getTotalPlayers();
        if (totalPlayers - loadedGames.get(i).getPlayingPlayers().size()>0  &&  loadedGames.get(i).getStatus().equals("not_started") )
              return true;
        return false;
    }

    public void deleteUserFromUsersList(String username){

        if (connectedPlayers.containsKey(username))
            connectedPlayers.remove(username);
    }

    public void deleteUserFromGamePlayers(String username, int engineIndex) {
         loadedGames.get(engineIndex).removePlayerByName(username);
    }

    public void resetGame(Integer index) {

        Engine engine = loadedGames.get(index);

        engine.loadGame(engine.getInputStream(), 0,engine.getUserName());
        engine.initVariables();
        engine.initDictionaryAndManager(engine.getInputStreamDictionary(),engine.getDictionaryName());
    }
}
