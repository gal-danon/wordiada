//This function is called when board page is showed
$(function() {
    //The users list is refreshed automatically every 2 seconds
    setInterval(ajaxCurrentPlayersList, refreshRate);
    setInterval(ajaxCurrentStatus, 500);

    //The chat content is refreshed only once (using a timeout) but
    //on each call it triggers another execution of itself later (1 second later)
    ajaxCurrentPlayersList();

    ajaxUpdateGeneralInfoDiv();

});

var refreshRate = 1000;

function ajaxUpdateGeneralInfoDiv(){
    $.ajax(
        {
            url: "/wordiada/updateGeneralInfoDiv",
            success: function(info) {
                $('.gameType').text("Game Type: "+ info[0]);
                $('.goldFishMode').text("GoldFish Mode: "+info[1]);
                $('.totalTurnsUntilNow').text("Total Turns Until Now: "+info[2]);
                $('.boardSize').text("Board Size: "+info[3]+"x"+info[3]);
                $('.userNameSpan').text("Hi "+info[4] + "!");
            }

        });
}

function rollDice() {
    $.ajax({
        url: "/wordiada/rollDice"});
}

function selectLetter(name) {
    $.ajax(
        {
        data: {
            "":name
        },
        url: "/wordiada/selectLetter",
        success: function(letters) {
            if (letters != null) {
                showChosenLetters(letters);
            }
        }

    });
}

function ajaxEndGameDialog(){
    $.ajax(
        {
            url: "/wordiada/endGame",
            success: function(list) {
                if (list != null) {
                    $('.endGameDialog')[0].style.display = "inline-block";

                    var body = $('.endGameBody');
                    body.contents().remove();

                    for (i=0; i<list.length; i++)
                    {
                        var text = list[i];
                        var div = $(document.createElement('div')).text(text);
                        div.appendTo(body);
                    }
                }
            }

        });
}


function removeEndGameDialog(){
    $('.endGameBody')[0].style.display = "none";
}

function showChosenLetters(letters) {

    if (letters.message != null) {
        alert(letters.message);
    }
    else {
        $("#chosenWord").text(letters.word);
    }

}

function ajaxCurrentPlayersList() {
    $.ajax({
        url: "/wordiada/refreshPlayersCurrentGame",
        success: function(players) {
            refreshPlayersCurrentGame(players);
        }
    });
}

function refreshPlayersCurrentGame(players) {
    //clear all current games
    $("#currentPlayersTable tbody").empty();

        $.each(players || [], function(index, currentPlayer) {
        $('#currentPlayersTable tbody').append('<tr class="child"><td>' + currentPlayer[0] + '</td>' + '<td>' +
            currentPlayer[1] + '</td>' + '<td>' + currentPlayer[2] +
            '</td></tr>');
    });
}

function ajaxCurrentStatus() {
    $.ajax({
        url: "/wordiada/refreshGameStatus",
        success: function(status) {
            $("#rollit").off('click');
            refreshGameStatus(status);
        }

    });
}

function onDiscoverTilesClick() {
    $.ajax({
        url: "/wordiada/discoverTiles",
        success: function(board) {
            buildBoard(board, true);
        }

    });
}

function buildBoard(board, myTurn) {

    // Need to create the board
    $("#boardBody").empty();
    var length = board.size;
    var table = document.createElement('table');
    var tableBody = document.createElement('tbody');
    table.setAttribute("id", "boardTable");
    var counter = 0;
    for (var i=0; i<length; i++) {
        var row = document.createElement('tr');
        for (var j=0; j<length; j++) {
            var cell = document.createElement('td');

            var button = document.createElement('button');
            button.setAttribute('type', 'button');
            button.setAttribute('name', i + 'a' + j);
            button.setAttribute('id', i + 'a' + j);
            button.setAttribute('class', 'button');
            if (myTurn) {
                button.setAttribute('onclick', 'selectLetter(this.name)');
            }
            button.innerHTML = board.array[counter];
            cell.appendChild(button);
            row.appendChild(cell);
            counter = counter + 1;
        }

        tableBody.appendChild(row);
    }

    table.appendChild(tableBody);
    $("#boardBody").append(table);
}

function onCheckWordClick() {
    $.ajax({
        url: "/wordiada/checkWord",
        success: function(wordResult) {
            $("#chosenWord").text("");
            alert(wordResult);
        }
    });
}

function ajaxUpdatePlayerInfoDiv(){
    $.ajax(
        {
            url: "/wordiada/updatePlayerInfoDiv",
            success: function(info) {
                $('.playerName').text("Name: "+ info[0]);
                $('.playerType').text("Type: "+info[1]);
                $('.playerScore').text("Score: "+info[2]);
                $('.playerWordsCount').text("Words Count: "+info[3]);
                $('.currentMove').text(info[4]);
                $('.totalMoves').text(info[5]);

                //update in info div -currTurn
                $('.totalTurnsUntilNow').text("Total Turns Until Now: "+info[6]);
            }

        });
}

function refreshGameStatus(status) {

    var myTurn = status.myTurn;
    var message = status.msg;
    var action = status.action;
    var isComp = status.isComp;
    var boardOfStatus = status.board;
    $("#actionDiv").empty();

    ajaxUpdatePlayerInfoDiv();

    switch(action)
    {
        case "not_started":

            $("#msg").text("Waiting for players...")
            break;

        case "started":
            $("#msg").text(message);
            buildBoard(boardOfStatus, myTurn);
            if(myTurn && !isComp){

                $("#leaveForm").empty();
                var leave = document.createElement("input");
                leave.setAttribute("id", "leaveGame");
                leave.setAttribute("name", "leaveGame");
                leave.setAttribute("value", "LeaveGame");
                leave.setAttribute("type", "submit");
                $("#leaveForm").append(leave);

                var div = document.createElement('div');
                div.setAttribute("class", "action");
                div.setAttribute("onclick", "rollDice()");
                div.innerHTML ="Roll Dice" ;
                $("#actionDiv").append(div);
            }
            else
            //update player turn div
            ajaxUpdatePlayerInfoDiv();

            //check if the current player has retired, and one player left
            ajaxEndGameDialog();

            if (isComp && myTurn) {
                $.ajax(
                    {
                        url: "/wordiada/computerServlet"
                    });
            }
          break;

        case "roll":
            $("#msg").text(message);
            if(myTurn && !isComp){

                $("#leaveForm").empty();

                $("#actionDiv").empty();
                var div = document.createElement('div');
                div.setAttribute("class", "action");
                div.setAttribute("onclick", "onDiscoverTilesClick()");
                div.innerHTML ="Discover Tiles" ;
                $("#actionDiv").append(div);
            }
            break;
        case "discover":
            $("#msg").text(message);

            if(myTurn && !isComp){
                $("#actionDiv").empty();
                var div = document.createElement('div');
                div.setAttribute("class", "action");
                div.setAttribute("onclick", "onCheckWordClick()");
                div.innerHTML ="Check Word" ;
                $("#actionDiv").append(div);
            }
            buildBoard(boardOfStatus, myTurn);
            break;
        case "secondTry": {
            $("#msg").text(message);

            if(myTurn && !isComp){
                $("#actionDiv").empty();
                var div = document.createElement('div');
                div.setAttribute("class", "action");
                div.setAttribute("onclick", "onCheckWordClick()");
                div.innerHTML ="Check Word" ;
                $("#actionDiv").append(div);
            }
            buildBoard(boardOfStatus, myTurn);
            break;
        }
        case "finishTurn":
            ajaxEndGameDialog();
            ajaxUpdatePlayerInfoDiv();
            $("#msg").text(message);

            buildBoard(boardOfStatus, myTurn);
            $.ajax(
                {
                    url: "/wordiada/setStarted"
                });
            break;
        case "endGame":
            ajaxRedirectAllPlayersToDashboard();
            break;
    }
}

function ajaxRedirectAllPlayersToDashboard(){
    $.ajax({
            url: '/wordiada/redirectAllPlayersToDashboard',
            type: 'GET',
            success: function(index) {
                ajaxResetGame(index);
            }
        }
    )
}

function ajaxResetGame(index){
    $.ajax({
        data: {
            "":index
        },
        url: '/wordiada/resetGame',
        type: 'GET',
        }
    )
}

function onWordCollectionClick() {
    $.ajax({
            url: '/wordiada/wordCollection',
            type: 'GET',
            success: function(words) {
                $('.wordCollectionDialog')[0].style.display = "inline-block";

                var body = $('.wordCollectionBody');
                body.contents().remove();

                if (words.length === 0)
                {
                    var text = 'There are no words in the player words collection yet ..';
                    var div = $(document.createElement('div')).text(text);
                    div.appendTo(body);
                }

                for (i=0; i<words.length; i++)
                {
                    var text = words[i];
                    var div = $(document.createElement('div')).text(text);
                    div.appendTo(body);
                }
        }
        }
    )
}

function removeWordCollectionDialog() {
    $('.wordCollectionDialog')[0].style.display = "none";
}

function onCashierStatusClick() {
    $.ajax({
            url: '/wordiada/cashierStatus',
            type: 'GET',
            success: function(lettersStatus) {
                $('.cashierStatusDialog')[0].style.display = "inline-block";

                var body = $('.cashierStatusBody');
                body.contents().remove();

                for (i=0; i<lettersStatus.length; i++)
                {
                    var text = lettersStatus[i];
                    var div = $(document.createElement('div')).text(text);
                    div.appendTo(body);
                }
            }
        }
    )
}

function removeCashierStatusDialog(){
    $('.cashierStatusDialog')[0].style.display = "none";
}

function onTenWordsClick() {
    $.ajax({
            url: '/wordiada/tenWords',
            type: 'GET',
            success: function(words) {
                $('.tenWordsDialog')[0].style.display = "inline-block";

                var body = $('.tenWordsBody');
                body.contents().remove();

                for (i=0; i<words.length; i++)
                {
                    var text = words[i];
                    var div = $(document.createElement('div')).text(text);
                    div.appendTo(body);
                }
            }
        }
    )
}

function removeTenWordsDialog(){
    $('.tenWordsDialog')[0].style.display = "none";
}

