<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 15/07/2017
  Time: 09:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Wordiada</title>
    <script src="jquery-2.0.3.min.js"></script>
    <script src="dashboard.js"></script>
    <meta charset="utf-8"/>
    <link rel="stylesheet" type="text/css" href="dashboardStyle.css">
</head>
<body class="body">
<div style="min-width: 960px; margin: 0 auto;">
<div class="toolBar">
    <div class="toolbarContent">
        <div class="titleDashboard" >
            <span> <h1 style=" text-align: center;">Wordiada Dashboard</h1></span>
        </div>

        <form class="logoutForm" action="${pageContext.request.contextPath}/logout" method="get">
            <input type="submit" id="logout" name="logout" value="Logout" style="" />
        </form>
        <span class="userNameSpan"></span>
        </span>
    </div>
</div>
<div class="mainDiv">
    <div class="previewGameDialog">
        <h2>Board Preview:</h2>
        <div class="previewGameBody" onclick="removePreviewGameDialog()">

        </div>
    </div>
<div class="gamesDiv">
    <div class="gamesActions">
<form class="fileInput" action="filesUpload" enctype="multipart/form-data" method="POST">
    <span style="display:inline-block; width: 130px; font-family: arial, sans-serif;">Choose Xml</span><input type="file" name="file1" value="Choose Xml"><br>
    <span style="display:inline-block; width: 130px; font-family: arial, sans-serif;">Choose Dictionary</span><input type="file" name="file2" value="Choose Dictionary"><br>
    <br/>
    <input type="Submit" value="Upload Game"><br>
    <% Object errorMessage = request.getAttribute("FileErrorMsg");%>
    <% if (errorMessage != null) {%>
    <span class="bg-danger" style="color:red;"><%=errorMessage%></span>
    <% } %>
</form>
    </div>

<form  action="joinGame" enctype="multipart/form-data" method="GET">
    <h3>List Of Games:</h3>
    <div class="responsiveTable">
    <table class="gamesTable" id="gamestable" width="100px">
        <thead>
        <tr style="font-size: 76%">
            <th>Game Name</th>
            <th>Uploaded By User</th>
            <th>Board Size</th>
            <th>Register Players/Needed Players</th>
            <th>Dictionary</th>
            <th>Letters In Deck</th>
            <th>Join Game</th>
            <th>Preview Game</th>
            <th>Game Status</th>
        </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
    </div>
</form>

</div>
<div class="usersDiv">
<h3>Online Users:</h3>
<ol class="usersList" id="usersList">
</ol>
</div>
</div>
</div>
</body>
</html>
