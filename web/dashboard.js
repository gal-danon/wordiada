var refreshRate = 2000;

function ajaxGamesList() {
        $.ajax({
            url: "/wordiada/refreshGames",
            success: function(games) {
                refreshGamesList(games);
            }
        });
}

function ajaxUsersList() {
    $.ajax({
        url: "/wordiada/refreshUsers",
        success: function(users) {
            refreshUsersList(users);
        }
    });
}

function refreshGamesList(games) {
    //clear all current games
    $("#gamestable tbody").empty();

    $.each(games || [], function(index, currentgame) {
        $('#gamestable tbody').append('<tr class="child"><td>' + currentgame[0] + '</td>' + '<td>' +
            currentgame[1] + '</td>' + '<td>' + currentgame[2] + '</td>' + '<td>' + currentgame[3] + '</td>' + '<td>' +
            currentgame[4] + '</td>' +'<td>' + currentgame[5] + '</td>'+
            '<td>' + '<input type="submit" id="'+ index + '" name="'+index+'" value="Join Me!" formaction="joinGame" aria-atomic="" style="background-color: white; color: black; border: 2px solid #4CAF50; border-radius: 8px;" ></td>'+
            '<td>' + '<div  value="preview" id="'+ index + '" name="'+index+'" onclick="onPreviewClick(' + index + ')" aria-atomic="" style="background-color: white; text-align: center; color: black; border: 2px solid #4CAF50; border-radius: 8px;" >Preview</div></td>'+
            '<td>' +  currentgame[6] + '</td></tr>');
    });
}

function onPreviewClick(index) {
    $.ajax({
        data: {
            "":index
        },
        url: "/wordiada/previewGame",
        success: function(size) {
            $('.previewGameDialog')[0].style.display = "inline-block";

            // Need to create the board
            var body = $('.previewGameBody');
            body.contents().remove();
            var length = size;
            var table = document.createElement('table');
            var tableBody = document.createElement('tbody');
            table.setAttribute("id", "boardTable");
            for (var i=0; i<length; i++) {
                var row = document.createElement('tr');
                for (var j=0; j<length; j++) {
                    var cell = document.createElement('td');

                    var button = document.createElement('button');
                    button.setAttribute('type', 'button');
                    button.setAttribute('name', i + 'a' + j);
                    button.setAttribute('id', i + 'a' + j);
                    button.setAttribute('class', 'button');
                    button.innerHTML = '?';
                    cell.appendChild(button);
                    row.appendChild(cell);
                }

                tableBody.appendChild(row);
            }

            table.appendChild(tableBody);
            $("#boardBody").append(table);
            var div = $(document.createElement('div')).append(table);
            div.appendTo(body);
        }
    });
}

function removePreviewGameDialog(){
    $('.previewGameDialog')[0].style.display = "none";
}

function refreshUsersList(users) {
    //clear all current users
    $("#usersList").empty();

    $.each(users || [], function(index, user) {
        $('#usersList').append('<li>' + user + '</li>');
    });
}

//activate the timer calls after the page is loaded
$(function() {
    //prevent IE from caching ajax calls
    $.ajaxSetup({cache: false});

    //The users list is refreshed automatically every 2 seconds
    setInterval(ajaxGamesList, refreshRate);

    setInterval(ajaxUsersList, refreshRate);

    //The chat content is refreshed only once (using a timeout) but
    //on each call it triggers another execution of itself later (1 second later)
    ajaxGamesList();
    ajaxUsersList();
});

// check if localStorage available
function lsTest(){
    var test = 'test';
    try {
        localStorage.setItem(test, test);
        localStorage.removeItem(test);
        return true;
    } catch(e) {
        return false;
    }
}

// listen to storage event
window.addEventListener('storage', function(event){
    // do what you want on logout-event
    if (event.key == 'logout-event') {
        $('#console').html('Received logout event! Insert logout script here.');
         window.location = "/wordiada/logout";
    }
}, false);

$(document).ready(function() {
    if (lsTest()) {
        $('#logout').on('click', function () {
            // change logout-event and therefore send an event
            localStorage.setItem('logout-event', 'logout' + Math.random());
            return true;
        });
    } else {
        setInterval(ajaxUsersList, refreshRate);
    }
});