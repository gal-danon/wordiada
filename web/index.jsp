<!DOCTYPE html>
<html>
<% if(session.getAttribute("username")!=null ) {%>
       <% response.sendRedirect("dashboard.jsp");%>
<% }  %>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <title>Wordiada</title>

  <!-- Link the Bootstrap (from twitter) CSS framework in order to use its classes-->
  <link rel="stylesheet" href="../../common/bootstrap.min.css">
    <!-- Link jQuery JavaScript library in order to use the $ (jQuery) method-->
  <!-- <script src="common/jquery-2.0.3.min.js"></script>-->
  <!-- and\or any other scripts you might need to operate the JSP file behind the scene once it arrives to the client-->
 <!-- <style> -->
  <!--   h1,h2,h3{ -->
  <!--  font-family: arial, sans-serif; -->
  <!--}</style> -->
</head>
<link rel="stylesheet" type="text/css" href="loginStyle.css">
<body>
<div style="min-width: 960px; margin: 0 auto;">
<div class="login-wrap">
    <div class="login-html">
        <div class="login-form">
            <div class="sign-in-htm">

                <input id="tab-1" type="radio" name="tab" class="sign-in" checked="">
                <label for="tab-1" class="tab">Wordiada</label>
                <h1>Please enter a unique </h1>
                <h1> user name:</h1>
              <br/>
              <form method="GET" action="login">

                  <div class="group">
                      <label for="user" class="label">Username</label>
                      <input id="user" name="username" type="text" class="input">
                  </div>

                  <div class="group">
                      <input id="check" type="checkbox" name="isComputer" class="check" checked="false">
                      <label for="check"><span class="icon"></span> computer</label>
                  </div>

                  <div class="group">
                      <input type="submit" class="button" value="Login"/>
                  </div>

                  <% Object errorMessage = request.getAttribute("errorMessage");%>
                  <% if (errorMessage != null) {%>
                  <span class="bg-danger" style="color:red;"><%=errorMessage%></span>
                  <% } %>


              </form>
            </div>
        </div>
    </div>
</div>
</div>
</body>
</html>