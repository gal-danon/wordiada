<%@ page import="servlets.JoinGameServlet" %><%--
  Created by IntelliJ IDEA.
  User: user
  Date: 20/07/2017
  Time: 20:06
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Wordiada</title>
        <script src="jquery-2.0.3.min.js"></script>
        <script src="board.js"></script>
        <meta charset="utf-8"/>
        <link rel="stylesheet" type="text/css" href="boardStyle.css">
</head>
<body length="<%=request.getSession().getAttribute("boardsize")%>">
<div style="min-width: 960px; margin: 0 auto;">
<div class="toolBar">
    <div class="toolbarContent">
        <span class="leaveGame" style="float: right">
            <form style="margin-left: 10px;" class="leaveGameForm" action="${pageContext.request.contextPath}/leaveGame" id="leaveForm" method="get">
            </form>
        </span>
        <span id="userNameSpan" class="userNameSpan" style="margin-left: 100px"> </span>
        <span class="gameStatus" id="msg"></span>
    </div>
</div>
<div class="mainDiv">

    <div class="wordCollectionDialog">
        <h2>Player Word Collection:</h2>
        <div class="wordCollectionBody" onclick="removeWordCollectionDialog()">

        </div>
    </div>
    <div class="cashierStatusDialog">
        <h2>Cashier Status:</h2>
        <div class="cashierStatusBody" onclick="removeCashierStatusDialog()">

        </div>
    </div>
    <div class="tenWordsDialog">
        <h2>10 Words With Lowest Frequency:</h2>
        <div class="tenWordsBody" onclick="removeTenWordsDialog()">

        </div>
    </div>
    <div class="endGameDialog">
        <h2>GAME OVER!:</h2>
        <div class="endGameBody" onclick="removeEndGameDialog()">

        </div>
    </div>
    <div class="mainDivBody">
    <div>
        <h2 class="PlayerTurn">Player Turn: </h2>
        <h2 class="currentPlayerName"></h2>
    </div>
    <div class="leftDiv">
        <div class="details">
            <div class="playerName">Name:</div>
            <div class="playerType">Type:</div>
            <div class="playerScore">Score:</div>
            <div class="playerWordsCount">Words Count:</div>
        </div>
        <div class="action" onclick="onWordCollectionClick()">Word Collection</div>
        <div class="actions" id="actionDiv">

        </div>
    </div>
    <form  method="GET" action="login" style="margin-right: -200px; display: inline-block;">
        <span class="gameStatus" id="chosenWord"></span>
        <div class="board" id="board">
            <div class="boardBody" id="boardBody">

            </div>
        </div>
    </form>
    <div class="rightDiv">
    <div class="players">
        <table class="gamesTable" id="currentPlayersTable" width="100px" style="text-align: center; width: 100%;">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Type</th>
                    <th>Score</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>

        <div  id="generalInfo" class="generalInfo">
        <div class="gameType">Game Type:</div>
        <div class="goldFishMode">GoldFish Mode:</div>
        <div class="totalTurnsUntilNow">Total Turns Until Now:</div>
        <div class="boardSize">Board Size:</div>
            <div value="/n">
            </div>
        <div class="generalInfoButtons" >
        <div class="actions" disable = "true">
            <div class="action" onclick="onCashierStatusClick()">Cashier Status</div>
            <div class="action" onclick="onTenWordsClick()">10 Words With Lowest Frequency</div>
        </div>
        </div>
    </div>
    </div>
    </div>
</div>
</div>
</div>
</body>
</html>
